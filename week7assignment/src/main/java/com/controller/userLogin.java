package com.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDao;
import com.user.User;

@WebServlet("/userLogin")
public class userLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static User loginUser;
    private UserDao userDao;
 public void init() {
 	try {
			userDao = new UserDao();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
 }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName;
		String password;
		userName = request.getParameter("loginname");
		password = request.getParameter("loginpwd");
		
		
		
		try {
			if(userDao.getUser(userName,password)) {
				loginUser = new User();
				loginUser.setUserName(userName);
				loginUser.setPassword(password);
				response.sendRedirect("LoginSuccess.jsp");
				
			}else {
				response.sendRedirect("LoginFail.jsp");
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}

}
