package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.user.User;

public class UserDao {
	private Connection con;	
	private PreparedStatement statement;
	public UserDao() throws ClassNotFoundException, SQLException {
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore5","root","Deepika@33");
		System.out.println("Connection Established...");
	}catch(Exception e) {
		e.printStackTrace();
	}
}
	public void userRegisteration(User reg) throws SQLException {
		try {
		statement = con.prepareStatement("insert into user values(?,?)");
		statement.setString(1, reg.getUserName());
		statement.setString(2, reg.getPassword());
		}catch(Exception e) {
			e.printStackTrace();
		}	
	}
	public boolean getUser(String userName, String password)throws SQLException {
		statement = con.prepareStatement("select *from user");
		ResultSet rs = statement.executeQuery();
		while(rs.next()) {
			if(rs.getString(1).equals(userName) && rs.getString(2).equals(password)) {
				statement.close();
				return true;
			}
			}
			statement.close();
			return false;
		}	
	public void addLikedBooks(String name, String password, String bookId)throws SQLException {
		statement = con.prepareStatement("insert into likedBook(userName,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from liked");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
			}
			statement.setString(1,name);
			statement.setString(2,password);
			statement.setString(3,bookId);
			statement.executeUpdate();	
			statement.close();	
			st2.close();
			rs.close();	
		}
	public void addLaterBooks(String name, String password, String bookId)throws SQLException {
		statement = con.prepareStatement("insert into ReadLater(name,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from ReadLater");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
		}
		statement.setString(1, name);
		statement.setString(2, password);
		statement.setString(3, bookId);
		statement.executeUpdate();
		statement.close();
		st2.close();
		rs.close();	
	}	

}
