import { Component, OnInit } from '@angular/core';
import {FormBuilder ,FormGroup} from '@angular/forms'
import { ApiService } from '../shared/api.service';
import { BookModel } from './book-dashboard.model';

@Component({
  selector: 'app-bookdashboard',
  templateUrl: './bookdashboard.component.html',
  styleUrls: ['./bookdashboard.component.css']
})
export class BookdashboardComponent implements OnInit {
  formValue !: FormGroup;
  bookModelobj :  BookModel = new BookModel();
  bookData !:any;
  showAdd !:boolean;
  showUpdate !:boolean;

  constructor(private formbuilder : FormBuilder,
    private api :ApiService) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      bookName:[''],
      author:[''],
      year : ['']
    })
    this.getAllBooks();
  }
  clickAddBook(){
    this.formValue.reset();
    this.showUpdate=false;
    this.showAdd = true;
  }
  postBookDetails(){
    this.bookModelobj.bookName = this.formValue.value.bookName;
    this.bookModelobj.author = this.formValue.value.author;
    this.bookModelobj.year = this.formValue.value.year;
    this.api.postBook(this.bookModelobj).subscribe(res => {
        console.log(res);
        alert("book added sucessfully");
        this.formValue.reset();
        let ref = document.getElementById("cancle");
        ref?.click();
        this.formValue.reset();
        this.getAllBooks();
      },
     
      
      )
  }
  getAllBooks(){
    this.api.getBook().subscribe(res=>{
      this.bookData =res;
    })
  }
  deletBook( row : any){
    this.api.deleteBook(row.id).subscribe(res=>{
      alert("book deleter");
      this.getAllBooks();
    })
    
  }
onEdit(row :any){
  this.showAdd = false;
  this.showUpdate = true;
  this.bookModelobj.id = row.id;
  this.formValue.controls['bookName'].setValue(row.bookName);
  this.formValue.controls['author'].setValue(row.author);
  this.formValue.controls['year'].setValue(row.year);
}
updateBookDetails(){
  this.bookModelobj.bookName = this.formValue.value.bookName;
  this.bookModelobj.author = this.formValue.value.author;
  this.bookModelobj.year = this.formValue.value.year;
  this.api.updateBook(this.bookModelobj,this.bookModelobj.id).subscribe(ras =>{
    alert("updated succesfully");
    let ref = document.getElementById("cancle");
    ref?.click();
    this.formValue.reset();
    this.getAllBooks();
  })
}
}
