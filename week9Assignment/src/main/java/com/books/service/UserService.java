package com.books.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.books.bean.user;
import com.books.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	public List<user>getAllUsers(){
		return userDao.findAll();
	}
	public String storeUserInfo(user user) {
		if(userDao.existsById(user.getId())) {
			return " user Id must be unique";
		}else {
			userDao.save(user);
			return "userData saved successfully";
		}
		
	}
	public String updateUserInfo(user user) {
		if(!userDao.existsById(user.getId())) {
			return "userData is not present";	
		}else {
			user u=userDao.getById(user.getId());
			u.setUsername(user.getUsername());
			userDao.saveAndFlush(u);
			return "username is updated succesfully";
		}
	}
public String DeleteUserInfo(int uid){
	if(!userDao.existsById(uid)) {
		return "userData is not present";
	}else {
		userDao.deleteById(uid);
		return "userData delete successfully";
	}
}
}
