package com.books.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.books.bean.user;

public interface UserDao extends JpaRepository<user,Integer> {

}
