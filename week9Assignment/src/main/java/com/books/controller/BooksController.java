package com.books.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.books.bean.Books;
import com.books.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {
	@Autowired
	BooksService booksService;
	@GetMapping(value="getAllBooks",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Books>getAllBooksInfo(){
		return booksService.getAllBooks();
	}
	@PostMapping(value="storeBooks",consumes=MediaType.APPLICATION_JSON_VALUE)
 public String storeBooksInfo(@RequestBody Books book) {
		return booksService.storeBooksInfo(book);
	}
	@PatchMapping(value="updateBooks")
	public String updateBooksInfo(@RequestBody Books book) {
		return booksService.updateBooksInfo(book);
	}
	@DeleteMapping(value="deleteBooks/{bid}")
	public String DeleteBooksInfo(@PathVariable("bid")int bid) {
		return booksService.DeleteBooksInfo(bid);
		
	}
}
