package com.books.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.books.bean.user;
import com.books.service.UserService;

@RestController
@RequestMapping("/User")
public class UserController {
	@Autowired
	UserService userService;
	@GetMapping(value="getAllUser",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<user>getAllUsersInfo(){
		return userService.getAllUsers();
	}
	@PostMapping(value="storeUser",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody user user) {
		return userService.storeUserInfo(user);
	}
    @PatchMapping(value="updateUser")
    public String updateUserInfo(@RequestBody user user) {
    	return userService.updateUserInfo(user);
    }
    @DeleteMapping(value="deleteUser/{uid}")
    public String DeleteUserInfo(@PathVariable("uid") int uid) {
    	return userService.DeleteUserInfo(uid);
    }
}
