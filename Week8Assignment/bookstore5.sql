create database bookstore5;
use bookstore5;
create table books(id int primary key,title varchar(50),author varchar(50),genre varchar(50));
insert into books values(1,"Every One Has A Story","savi sharama","Romantic");
insert into books values(2,"The Everyday Hero","Robin Sharma","Fantasy");
insert into books values(3,"The Fast Five","Eina Blython","Novel");
insert into books values(4,"This is my story","savi sharama","Romantic");
insert into books values(5,"Warmth","Rithvik","Narrative");
create table bookUser(username varchar(30) primary key ,
password varchar(30)
);
create table Likedbooks(id int primary key,title varchar(50),author varchar(50),genre varchar(50));
create table ReadLaterbooks(id int primary key,title varchar(50),author varchar(50),genre varchar(50));


