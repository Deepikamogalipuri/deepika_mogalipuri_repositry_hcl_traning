<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page import="java.util.List"%>
<%@ page import="com.spring.books.*"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<title>view book page</title>
</head>
<body>
       
		<h1 style="font-size: 80px">Welcome To The Book Store</h1>
		<p>
			<a href="login" style="font-size: 30px">Login</a>
		</p>
		<p>
			<a href="registration" style="font-size: 30px">New Registration</a>
		</p>
		<p>
			<a href="index" style="font-size: 30px">Go To Index</a>
		</p>
		<table border="1" style="background-color: #BDB76B">
			<tr>
				<td>Book_ID</td>
				<td>Book_Name</td>
				<td>Book_Author</td>
				<td>Book_Genre</td>
			</tr>


			<%
			@SuppressWarnings("unchecked")
			List<Books> book = (List<Books>) request.getAttribute("list");
			%>

			<%
			for (Books b : book) {
			%>

			<tr>
				<td><%=b.getBook_id()%></td>
				<td><%=b.getTitle()%></td>
				<td><%=b.getAuthor()%></td>
				<td><%=b.getGenre()%></td>
				<form action="like.spring">
				<input type="hidden"name="book_id" value=<%b.getBook_id();%>>
				<input type="submit" value="Like">
			</tr>
			<%
			}
			%>
		</table>
</body>
</html>