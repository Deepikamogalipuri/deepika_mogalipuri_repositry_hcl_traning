<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ page import="java.util.List"%>
<%@ page import="com.spring.books.*"%>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>welcome Page</title>
</head>
<body>
		<%
		String username = (String) request.getAttribute("username");
		session.setAttribute("username", username);
		%>
		
		<h1 style="font-size: 80px">Welcome To Library</h1>
		<h1>
			Hello
			<%=username%></h1>
		<p>
			<a href="viewReadLater" style="font-size: 30px">Read Later</a>
		</p>
		<p>
			<a href="viewLike" style="font-size: 30px">Like</a>
		</p>
		
			<form method="post" action="saveBooks">
				<table border="1" style="background-color: #BDB76B">
					<tr>
						<td>Book_ID</td>
						<td>Book_Name</td>
						<td>Book_author</td>
						<tb>Book_genre</tb>
						<td>Read_Later</td>
						<td>Liked_Book</td>
					</tr>
					<%
					@SuppressWarnings("unchecked")
					List<Books> book = (List<Books>) request.getAttribute("list");
					%>

					<%
					for (Books b : book) {
					%>

					<tr>
						<td><%=b.getBook_id()%></td>
						<td><%=b.getTitle()%></td>
						<td><%=b.getAuthor()%></td>
						<td><%=b.getGenre()%></td>
		<td><ahref="#"><button type="button">Read Later</button></a></td>
		<td><ahref="#"><button type="button">Like</button></a></td>
					</tr>
					<%
					}
					%>
				</table>
			</form>
</body>
</html>