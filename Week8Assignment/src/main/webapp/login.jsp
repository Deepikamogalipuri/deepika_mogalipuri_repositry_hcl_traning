<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Login Account</h1>
 <form:form id="loginForm" modelAttribute="login" action="logininfo" method="post">
                <table>
                    <tr>
                        <td>
                            <form:label path="username">userName: </form:label>
                        </td>
                        <td>
                            <form:input path="username" name="username" id="username" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="password">Password:</form:label>
                        </td>
                        <td>
                            <form:password path="password" name="password" id="password" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <form:button id="login" name="login">login</form:button>
                        </td>
                    </tr>
                </table>
            </form:form>
			<p>
             	<a href="registration">Create a new account</a>
             </p>
             <p>
           		<a href="index">Go To Indexpage </a>
           	 </p>
</body>
</html>