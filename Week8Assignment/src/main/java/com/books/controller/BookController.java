package com.books.controller;

import org.springframework.web.bind.annotation.RequestMapping;

public class BookController {
	@RequestMapping("/")
	public String start() {
		System.out.println("returning index page");
		return "index";
	}

	@RequestMapping("/index")
	public String index() {
		System.out.println("returning index page");
		return "index";
	}

	@RequestMapping("/registration")
	public String registration() {
		System.out.println("returning registration page");
		return "registration";
	}

}
