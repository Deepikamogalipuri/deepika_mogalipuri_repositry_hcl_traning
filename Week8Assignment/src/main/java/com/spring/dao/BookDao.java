package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.spring.books.Books;
@Repository
public class BookDao {
		@Autowired
		static
		JdbcTemplate jdbcTemplate;
       public static  List<Books> getBooks() {
			return jdbcTemplate.query("select * from books", new BooksRowMapper());
		}
	}
	class BooksRowMapper implements RowMapper<Books>{
		@Override
				public Books mapRow(ResultSet rs, int row) throws SQLException {
					Books b = new Books();
					b.setBook_id(rs.getInt(1));
					b.setTitle(rs.getString(2));
					b.setAuthor(rs.getString(3));
					b.setGenre(rs.getString(4));
					return b;
			}
	}

