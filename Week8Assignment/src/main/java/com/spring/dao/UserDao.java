package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.spring.books.User;
import com.spring.books.login;

@Repository
public class UserDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<User>getuserdetails(){
		return jdbcTemplate.query("select * from bookUser", new UserRowMapper());
	}
	public User validuserinfo(login login) {
		String s = "select * from users where username='" + login.getUsername() + "' and password='"
				+ login.getPassword() + "'";

		List<User> users = jdbcTemplate.query(s, new UserRowMapper());
		return  null;
	}
	public int Registerinfo(User user) {
		String s = "insert into users(username,,password) values('" + user.getUsername() + "','" + user.getPassword() + "')";
		return jdbcTemplate.update(s);
	}
	}
class UserRowMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs,int arg1)throws SQLException{
	User user=new User();
	user.setUsername(rs.getString("username"));
	user.setPassword(rs.getString("password"));
	return user;
}
}