package com.spring.books;

public class likedbooks {
	private int liked_book_id;
	private String user_name;
	private int book_id;
	private String title;
	private String author;
	private String genre;
	public int getLiked_book_id() {
		return liked_book_id;
	}
	public void setLiked_book_id(int liked_book_id) {
		this.liked_book_id = liked_book_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	

}
