package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.books.Books;
import com.spring.books.User;
import com.spring.books.login;
import com.spring.dao.BookDao;
import com.spring.dao.UserDao;

@Controller
public class LoginController {
	@Autowired
	UserDao userDao;
	@Autowired
	BookDao bookDao;
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("login", new login());

		return mav;
	}

	@RequestMapping(value = "/logininfo", method = RequestMethod.POST)
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
		@ModelAttribute("login") login login) {
		ModelAndView mav = null;
		User user = userDao.validuserinfo(login);
		if (null != user) {
			mav = new ModelAndView("welcome");
			List<Books> list = bookDao.getBooks();
			mav.addObject("list", list);
			mav.addObject("username", user.getUsername());
		} else {
			mav = new ModelAndView("login");
			mav.addObject("msg", "Username or Password is invalid!");
		}
		return mav;
	}
}

