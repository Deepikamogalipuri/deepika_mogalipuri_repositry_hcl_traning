package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.books.User;
import com.spring.dao.UserDao;
@Controller
public class RegisterController {
	@Autowired
	UserDao userDao;

	@RequestMapping(value = "/Registerinfo", method = RequestMethod.POST)
	public String s(@ModelAttribute("user") User user) {
		userDao.Registerinfo(user);
		return "index";
	}

}
