package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.books.Books;
import com.spring.dao.BookDao;

@Controller
@RequestMapping("/Books")
public class BooksController {
	@GetMapping(value="allBooks")
	public ModelAndView getAllBooks(HttpServletRequest req) {
		ModelAndView mav=new ModelAndView();
		List<Books> list = BookDao.getBooks();
		req.setAttribute("books",list);
		req.setAttribute("msg", "All Books Details");
		mav.setViewName("allBooks.jsp");
		return mav;
	}
	@GetMapping(value="likeBooks")
	public ModelAndView getlikeBooks(HttpServletRequest req) {
		ModelAndView mav=new ModelAndView();
		int book_id=Integer.parseInt(req.getParameter("book_id"));
		System.out.println("Book id is"+book_id);
		List<Books> list = BookDao.getBooks();
		req.setAttribute("books",list);
		req.setAttribute("msg", "All Books Details");
		mav.setViewName("allBooks.jsp");
		return mav;
	}
}
