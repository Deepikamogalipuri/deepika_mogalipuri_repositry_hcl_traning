package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.Service.MenuService;
import com.bean.Admin;
import com.bean.Menu;
@Controller
public class MenuController {
	@Autowired
	MenuService menuService;


	@GetMapping("/addMenu")
	public ModelAndView addMenu(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {
		
		ModelAndView mav = new ModelAndView("addMenu");
		mav.addObject("name", admin.getName());
		mav.addObject("menu", new Menu());
		
		return mav;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMenu(@ModelAttribute("menu") Menu menu, Model m) {

		menuService.addMenu(menu);

		

		String menuName = menu.getItem();
		m.addAttribute("menuName", menuName);

		List<Menu> list = menuService.viewmenu();
		m.addAttribute("list", list);

		return "addMenu";
	}

	@GetMapping("/deleteMenu/{id}")
	public String menuDelete(@PathVariable("itemid") int itemid) {

	menuService.DeleteMenu(itemid);

		return "deleteMenu";
	}

	@RequestMapping(value = "/adminPage", method = RequestMethod.GET)
	public ModelAndView showAdminPage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {

		ModelAndView mav = null;

		mav = new ModelAndView("adminPage");
		List<Menu> list = menuService.viewmenu();
		mav.addObject("list", list);
		mav.addObject("name", admin.getName());

		return mav;
	}

}
