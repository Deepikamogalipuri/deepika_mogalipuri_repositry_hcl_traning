package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bean.Menu;

public interface MenuDao extends JpaRepository<Menu,Integer>{
	

}
