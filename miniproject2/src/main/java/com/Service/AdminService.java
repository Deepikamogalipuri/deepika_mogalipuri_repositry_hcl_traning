package com.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;
@Service
public class AdminService {
@Autowired
AdminDao adminDao;
	public void save(Admin admin) {
		adminDao.save(admin);
	}	
	public Admin adminLogin(String name, String password) {
		Admin admin = adminDao.findByNameAndPassword(name, password);
	  	return admin;
}
}