package com.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Menu;
import com.dao.MenuDao;

@Service
public class MenuService {
	@Autowired
	MenuDao menuDao;
	public List<Menu> viewmenu() {
		List<Menu> menu = menuDao.findAll();
		return menu;
	}

	public void addMenu(Menu menu) {
		menuDao.save(menu);
		
	}

	public String DeleteMenu(int itemid) {
		if(!menuDao.existsById(itemid)) {
			return "menu info is not present";
		}else {
			menuDao.deleteById(itemid);
			return "menuitem Deleted successfully";
		}
	}
	
	
}
