package com.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.dao.UserDao;
@Service
public class UserLoginService {
	@Autowired
	UserDao userDao;
	public User login(String name, String password) {
		User user = userDao.findByNameAndPassword(name, password);
		return user;
	}
		public void save(User user) {
			userDao.save(user);
		}
		public List<User> showUser() {
			List<User> user = userDao.findAll();
			return user;
		}
}